
/**
 * @file
 * README file for Hydra Player.
 * @ingroup hydra_player
 */ 

 
Hydra Player is an helper module for dealing with Flash players.
At the moment it only supports (and requires) Flowplayer.
