
Drupal.hydraPlayer = {
  onLoad: null,
  onError: null,
  onBufferFull: null,
  onFinish: null
};

Drupal.behaviors.hydraPlayer = function() {
  config = Drupal.settings.hydraPlayer;
  Drupal.hydraPlayer.settings = {
    onLoad: Drupal.hydraPlayer.onLoad,
    onError: Drupal.hydraPlayer.onError,
    clip: config.clip || null,    
    playlist: config.playlist || null,    
    plugins: config.plugins || null,
    screen: config.screen || null,
    play: config.play || null
  };
  if (Drupal.hydraPlayer.settings.clip) {
    Drupal.hydraPlayer.settings.clip.onBufferFull = Drupal.hydraPlayer.onBufferFull;
    Drupal.hydraPlayer.settings.clip.onFinish = Drupal.hydraPlayer.onFinish;
  }
	Drupal.hydraPlayer.player = $f(config.selector, config.playerPath, Drupal.hydraPlayer.settings);
};
